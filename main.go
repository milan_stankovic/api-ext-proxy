package main

import (
	"api-ext-proxy/controller"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	v2 := r.Group("/api-ext/v2")
	{
		//v2.POST("/proxy", controller.ProxyController)

		v2.POST("/player-debit", controller.ProxyControllerDebit)
		v2.POST("/player-credit", controller.ProxyControllerCredit)
		v2.POST("/player-debit-undo", controller.ProxyControllerUndoDebit)
	}
	r.Run(":1898")
}
