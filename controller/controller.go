package controller

import (

	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/BurntSushi/toml"
	"log"
	"net/http"
	//"net/url"
	"strings"

	"bytes"
	"io/ioutil"
)

type ApiExtResponse struct {
	Success	bool	`json:"success" bson:"success"`
	Message	string	`json:"message" bson:"message"`
	ErrorType	int	`json:"errorType" bson:"errorType"`
	Coins	int	`json:"coins" bson:"coins"`
}


type PlayerDebitRequest struct {
	ApiKey      string      `json:"apiKey"`
	GameSession string      `json:"game_session"`
	Data        PlayerDebit `json:"data"`
	PlayerId 	string 		`json:"playerId"`
}

type PlayerDebit struct {
	MachineId          string `json:"machineId"`
	Amount             int64  `json:"amount"`
	Reason             string `json:"reason"`
	DebitTransactionId string `json:"debitTransactionId"`
}


type server struct {
	SUFFIX string
	SERVER string
}

type defaultServer struct {
	SERVER string
}

type tomlConfig struct {
	Default 	defaultServer
    Servers 	[]server
}



var config tomlConfig




func init() {

	log.SetPrefix("API_EXT-PROXY:  ")


	//if _, err := toml.DecodeFile("config/api-ext-proxy/proxy.toml", &config); err != nil {
	if _, err := toml.DecodeFile(/*"C:\\Users\\milan.stankovic\\Documents\\projects\\src\\config\\api-ext-proxy\\proxy.toml"*/"/etc/ghub/config/api-ext-proxy/proxy.toml", &config); err != nil {
		log.Println(err.Error())

		panic(err)
	}

	log.Println(config.Default)
}


/**
* Updates / increases player's coin amount and put in transaction in history in case that it is not presented in collection already	
*/
func ProxyController(c *gin.Context, route string) {

	log.Println("ProxyController: ")

	playerCreditRequest := PlayerDebitRequest{}

	if err := json.NewDecoder(c.Request.Body).Decode(&playerCreditRequest); err != nil {



		log.Println("ProxyController: " + err.Error())
		//log.Println("ProxyController body: " + c.Request.Body)

		c.JSON(http.StatusBadRequest, gin.H{"success": false,
			"message":   "Error in decoding request",
			"errorType": 1,
			"coins":     0})
		return
	}


	log.Println("request:")
	log.Println(playerCreditRequest)

	machineId := playerCreditRequest.Data.MachineId

	
	proxyURLString, err := proxyTo( &machineId, &config )
	if err != nil {
		log.Println( "Proxy error: " + err.Error() )
	}

    // proxyUrl, err := url.Parse( proxyURLString + route)
    // if err != nil {
    // 	log.Println( "proxyUrl parse error: " + err.Error() )
    // }


    // c.Request.req.RequestURI = ""
    // c.Request.URL = proxyUrl

	res1B, err := json.Marshal(&playerCreditRequest)
		if err != nil {
			//TODO: error handling?
			log.Println("ProxyController:" + err.Error())
			c.String(http.StatusBadRequest, "Error: "+err.Error())
			return
	}

	//log.Println(string(res1B))

	req, err := http.NewRequest("POST", proxyURLString + route, bytes.NewBuffer(res1B))
	if err != nil {
			//TODO: error handling?
			log.Println("ProxyController" + err.Error())
			c.String(http.StatusBadRequest, "Error: "+err.Error())
			return
	}

	log.Println(req)

    //client := &http.Client{ Transport: &http.Transport{ Proxy: http.ProxyURL( proxyUrl  ) } }
    client := &http.Client{ }
    resp, err := client.Do( req )
    if err != nil {

    	log.Println("ProxyController: "  + err.Error() )
    	

    	c.JSON(http.StatusBadRequest, gin.H{"success": false,
			"message":   "Error in decoding request"})

    } else {
    	defer resp.Body.Close()
        contents, err := ioutil.ReadAll(resp.Body)
    	
    	if err != nil {
    		log.Println("response read error: "+err.Error())
    	}
    	
    	log.Println(string(contents))
    	responseJson := ApiExtResponse{}
    	err = json.Unmarshal(contents,&responseJson)
    	if err != nil {
    		log.Println("Unmarshal error: "+string(contents))
    	}
    	
    	c.JSON( resp.StatusCode, responseJson )
    }
    
}

func ProxyControllerDebit(c *gin.Context) {
	log.Println( "ProxyControllerDebit" )
	ProxyController(c, "/player-debit")
}

func ProxyControllerCredit(c *gin.Context) {
	log.Println( "ProxyControllerCredit" )
	ProxyController(c, "/player-credit")
}

func ProxyControllerUndoDebit(c *gin.Context) {
	log.Println( "ProxyControllerUndoDebit" )
	ProxyController(c, "/player-debit-undo")
}



func proxyTo( machineId *string, data *tomlConfig ) ( string, error) {
	for _, s := range data.Servers {
		if strings.HasSuffix( *machineId, s.SUFFIX ) {
			return s.SERVER, nil
		}
	}
	return data.Default.SERVER, nil
}
